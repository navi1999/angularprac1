import { Component, OnInit,OnDestroy } from '@angular/core';

@Component({
  selector: 'app-ngdestroytest',
  templateUrl: './ngdestroytest.component.html',
  styleUrls: ['./ngdestroytest.component.scss']
})
export class NgdestroytestComponent implements OnInit,OnDestroy {

  constructor() { }
  addstorage()
  {
    localStorage.setItem('store',JSON.stringify('this is destroy'));
  }
  ngOnInit(): void {
  }
  ngOnDestroy(): void{
    localStorage.removeItem('store');
  }
}
