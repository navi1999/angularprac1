import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
  public receivedmessage:any='';
  receiveMessage($event:any)
  {
     this.receivedmessage=$event;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
