import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ngdestroytest1Component } from './ngdestroytest1.component';

describe('Ngdestroytest1Component', () => {
  let component: Ngdestroytest1Component;
  let fixture: ComponentFixture<Ngdestroytest1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ngdestroytest1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ngdestroytest1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
