import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ngdestroytest2Component } from './ngdestroytest2.component';

describe('Ngdestroytest2Component', () => {
  let component: Ngdestroytest2Component;
  let fixture: ComponentFixture<Ngdestroytest2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ngdestroytest2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ngdestroytest2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
